#include "tile.h"
#include <vector>

using dd = std::pair<double, double>;
using vdd = std::vector<dd>;

int main(){
  //vdd points = { {0.9, 0.65}, {0.7,0.8}, {0.6,0.7} };
  vdd points = { {0.1, 0.1}, {0.5,0.5}, {0.7,0.7}, {0.9,0.9} };
  square<double> S;
  S.insert(points);
  S.cover();
}

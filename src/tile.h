#ifndef _TILE_H
#define _TILE_H

#include <iostream>
#include <set>
#include <map>
#include <vector>
#include "xtl/xmeta_utils.hpp"

namespace Detail{
  template<class real, int dir>
  struct Point {
    real x, y;

    //From the dominance partial order.
    friend bool operator<=(const Point& lhs, const Point& rhs){
      static constexpr real EPSILON = std::numeric_limits<real>::epsilon();
      return (dir * (rhs.x - lhs.x) <= EPSILON) && (dir * (rhs.y - lhs.y) <= EPSILON);
    }
  };

  template<class real, int dir>
  class tile{
    using point = Detail::Point<real, dir>;
    static constexpr bool propogate = (dir == -1);
    using ret_type = typename std::conditional_t<propogate, tile<real, 1>, bool>;
    using xset = std::set<real, std::less<real>>;
    using yset = std::set<real, std::greater<real>>;
    static constexpr real INF = 1000 * dir;

    const point origin;
    xset xs;
    yset ys;

    template<class T>
    std::enable_if_t<std::is_fundamental_v<T>> update_with(T& output, real x, real y) {};

    template<class T>
    std::enable_if_t<! std::is_fundamental_v<T>> update_with(T& output, real x, real y) {
      output.insert(x, y);
    };

  public:
    tile(real x, real y) : origin({x, y}), xs ({x, INF}), ys ({y, INF}) {}

    ret_type insert(real x, real y)
    {
      typename xset::iterator it_x = xs.begin();
      typename yset::iterator it_y = ys.begin();
      const point p = {x, y};

      ret_type output = xtl::mpl::static_if<propogate>
      (
        [&](auto self){return tile<real, 1>(x, y);},
        [&](auto self){return true;}
      );

      while(it_x != xs.end() && it_y != ys.end()){
        point q = {*it_x, *it_y};

        if(q <= p){return output;} //True only if p is inside the fronteir.

        if(p <= q){
          it_x = xs.erase(it_x);
          it_y = ys.erase(it_y);
        } else{
          ++it_x;
          ++it_y;
        }

        if(it_x != xs.end()){
          update_with(output, *it_x, q.y);
        }
      }

      xs.insert(p.x);
      ys.insert(p.y);

      return output;
    }

    real area() const{
      typename xset::iterator it_x = xs.begin();
      typename yset::iterator it_y = ys.begin();

      real sum = 0;
      point p = {*it_x, *it_y};
      while(++it_x != xs.end() && ++it_y != ys.end()){
        sum += (*it_x - origin.x) * (*it_y - origin.y);
        if(propogate){
          sum -= (*it_x - origin.x) * (p.y - origin.y);
        } else{
          sum -= (p.x - origin.x) * (*it_y - origin.y);
        }
        p = {*it_x, *it_y};
      }

      return sum;
    }

    real max_rect() const{
      typename xset::iterator it_x = xs.begin();
      typename yset::iterator it_y = ys.begin();

      real record = 0;
      do{
        record = std::max(record, (*it_x - origin.x) * (*it_y - origin.y));
      } while(++it_x != xs.end() && ++it_y != ys.end());

      return record;
    }

    void print() const{
      if(propogate){
        std::cerr << "Outer:" << "[" << origin.x << " , " << origin.y << "] ";
      } else{
        std::cerr << "Inner:" << "[" << origin.x << " , " << origin.y << "] ";
      }
      typename xset::iterator it_x = xs.begin();
      typename yset::iterator it_y = ys.begin();
      for(; it_x != xs.end() && it_y != ys.end(); ++it_x, ++it_y){
        std::cout << "(" << *it_x << " , " << *it_y << ") ";
      }
      std::cout << "Area: " << area() << std::endl;
    }
  };
}

template<class real>
class square{
  using point = std::pair<real, real>;
  using vect_points = std::vector<point>;
  using pointset = std::multimap<real, point, std::greater<real>>;
  using tile = Detail::tile<real, -1>;
  pointset points;
public:
  square(){
    points.insert({0, point(0, 0)});
  }

  void insert(point pt){
    points.insert({pt.first + pt.second, pt});
  }

  void insert(const vect_points& pts){
    for(auto& pt : pts){
      insert(pt);
    }
  }

  real cover() const{
    tile upper(1, 1);
    real total = 0;
    for(auto&& [key, value] : points){
      total += upper.insert(value.first, value.second).max_rect();
      upper.print();
    }
    std::cout << total << std::endl;
    return total;
  }
};


#endif

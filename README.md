# Anchored Rectangle Packing

## Description

This repository contains a header only library with the implementation of the `TilePacking` algorithm described in Dumitrescu, Toth (2012). The header [tile.h](src/tile.h) exposes only the templated class Square, which is a storage class accpeting insertion of points in the square [0,1] x [0,1], along with a compute method which runs `TilePacking` on the points and returns the area coverage.

## Dependancies

I used a meta-programming utility library XTL (by QuantStack), which comes on most package managers with the xtensor library (I have previously contributed to this project :P).

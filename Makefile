CXXFLAGS = -std=c++17 -O3

all: src/main.cc src/tile.h
	$(CXX) $(CXXFLAGS) -oprog.elf src/main.cc
	./prog.elf

clean:
	-rm prog.elf
